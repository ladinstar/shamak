<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from hencework.com/theme/philbert/full-width-light/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 14:19:54 GMT -->
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Philbert I Fast build Admin dashboard for any platform</title>
	<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

  <!-- Jasny-bootstrap CSS -->
	<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-1-active pimary-color-green">

      <!-- Top Menu Items -->
  		  <?php include("_menu.html") ?>
  		<!-- /Top Menu Items -->

      <!-- Left Sidebar Menu -->
  		  <?php include("_sidebar.html") ?>
      <!-- /Left Sidebar Menu -->


		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">

				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Advert</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="index.html">Dashboard</a></li>
						<li><a href="#"><span>table</span></a></li>
						<li class="active"><span>basic table</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->


        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default card-view">
              <div class="panel-heading">
                <div class="pull-left">
                  <h6 class="panel-title txt-dark">Add Advert</h6>
                </div>
                <div class="pull-right">
                  <a href="entreprise_add.php">
                    <button class="btn btn-success btn-anim">
                      <i class="fa fa-plus"></i>
                      <span class="btn-text">Add entreprise</span>
                    </button>
                  </a>

                  <a href="publicite_liste.php">
                    <button class="btn btn-success btn-anim">
                      <i class="fa fa-file"></i>
                      <span class="btn-text">List of advert</span>
                    </button>
                  </a>
								</div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <div class="form-wrap">
                    <form>
                      <div class="form-group mt-30 mb-30">
                        <label class="control-label mb-10 text-left">Entreprise</label>
                        <select class="form-control">
                          <option>Santa Lucia</option>
                          <option>CIMENCAM</option>
                          <option>CRTV</option>
                          <option>DJANGO </option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label class="control-label mb-10 text-left">Title of advert</label>
                        <input type="text" class="form-control" name="nom" placeholder="Enter the title">
                      </div>

                      <div class="form-group">
                        <label class="control-label mb-10 text-left">Link</label>
                        <input type="text" class="form-control" name="lien" placeholder="Enter the website link">
                      </div>

                      <div class="form-group mb-30">
                        <label class="control-label mb-10 text-left">Image</label>
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                          <span class="input-group-addon fileupload btn btn-info btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
                          <input type="file" name="...">
                          </span> <a href="#" class="input-group-addon btn btn-danger btn-anim fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash"></i><span class="btn-text"> Remove</span></a>
                        </div>
                      </div>

                      <button class="btn btn-success btn-anim">
                        <i class="fa fa-plus"></i>
                        <span class="btn-text">Submit</span>
                      </button>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>2017 &copy; Philbert. Pampered by Hencework</p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->

		</div>
		<!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

	<!-- JavaScript -->

    <!-- jQuery -->
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
	<!-- Piety JavaScript -->
	<script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
	<script src="dist/js/peity-data.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>

	<!-- Owl JavaScript -->
	<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

	<!-- Switchery JavaScript -->
	<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>

	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>

</body>


<!-- Mirrored from hencework.com/theme/philbert/full-width-light/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 14:19:54 GMT -->
</html>
