<?php
if(isset($_POST['Sug']) AND $_POST['Sug']=="Sug"){
    if(isset($_POST['nomSug'],$_POST['emailSug'],$_POST['sujetSug'],$_POST['messageSug']) AND !empty($_POST['nomSug']) AND !empty($_POST['emailSug']) AND !empty($_POST['sujetSug']) AND !empty($_POST['messageSug'])){
        require_once "script/connexiondb.php";
        require_once "class/SugestionsManager.class.php";
        require_once "phpMailerTest.php";
        $SugestionsManager = New SugestionsManager($db);
        $sugestions = New Sugestions(array(
            "nomSug" => htmlspecialchars($_POST['nomSug']),
            "emailSug" => htmlspecialchars(strtolower($_POST['emailSug'])),
            "sujetSug" => htmlspecialchars($_POST['sujetSug']),
            "messageSug" => htmlspecialchars($_POST['messageSug']),
        ));
        if($SugestionsManager->add($sugestions)>0){
            smtpmailer( 'alladintroumba@gmail.com',htmlspecialchars(strtolower($_POST['emailSug'])), htmlspecialchars($_POST['nomSug']), htmlspecialchars($_POST['sujetSug']), htmlspecialchars($_POST['messageSug']));
            $message = "Your message has been sended !";
        }
        else{
            $message = "Error ! Please resend again";
        }
    }
    else{
        $message ="Please fill all the items";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>About Us - Shamak Allharamadji</title>

        <link href="images/favicon.svg" rel="shortcut icon" type="image/x-icon">
        <link href="css/css.css" rel="stylesheet" type="text/css">
        <link href="css/icon.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <style>
        .bas{
            margin-bottom:0px;
            padding:10px 30px 50px 30px;
        }
        .row.message{
            padding:50px;
            border-radius:10px;
        }
        
        @media only screen and (max-width: 900px) {
            .row.message{
                padding:20px;
            }
            .bas{
                margin-bottom:0px;
                padding:10px 15px 50px 15px;
            }
        }
        @media only screen and (max-width: 700px) {
            .row.message{
                padding:5px;
            }
            .bas{
                margin-bottom:0px;
                padding:10px 5px 40px 5px;
            }
        }
        
        @media only screen and (max-width: 500px) {
            .row.message{
                padding:5px;
            }
        }
        @media only screen and (max-width: 400px) {
            .row.message{
                padding:5px;
            }
            .bas{
                margin-bottom:0px;
                padding:10px 0px 20px 0px;
            }
        }
        </style>
    </head>
    <body>
        <div class="row black white-text haut" style="margin-bottom:0px;" align="center">
            <div align="center">
                <h5>DIRECTOR</h5>
                <h4>SHAMAK ALLHARAMADJI</h4>
            </div>
        </div>
        <nav class="nav-extended grey darken-1">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo">Shamak</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="videos.php">VIDEOS</a></li>
                    <li><a href="adversiting.php">ADVERSITING</a></li>
                    <li><a href="films.php">FILMS</a></li>
                    <li><a href="ourteam.php">OUR TEAM</a></li>
                    <li class="active"><a href="about.php">ABOUT US</a></li>
                    <li><a href="contact.php">CONTACT US</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="videos.php">VIDEOS</a></li>
                    <li><a href="adversiting.php">ADVERSITING</a></li>
                    <li><a href="films.php">FILMS</a></li>
                    <li><a href="ourteam.php">OUR TEAM</a></li>
                    <li class="active"><a href="about.php" class="h">ABOUT US</a></li>
                    <li><a href="contact.php" class="h">CONTACT US</a></li>
                </ul>
            </div>
        </nav>
        
        <div class="grey darken-2">
            <div style="padding:30px 0px" class="center white-text">
                <h4 style="font-weight:900">ABOUT US</h4>
            </div>
            <div class="row bas">
                    <div class="row card hoverable message" style="margin-bottom:0px">
                        <div class="row">
                            <div class="col s12 m8" style='font-weight:200px;line-height:3;text-align:justify'>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio inventore quis nihil voluptates ex amet recusandae, deleniti sint, accusamus beatae possimus officia voluptatum quaerat dolorem facilis excepturi dolore nemo quibusdam!
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis pariatur impedit adipisci ad delectus dolore nulla doloribus soluta quam provident, asperiores placeat ratione quas eos nihil esse possimus atque sunt.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis animi nisi hic pariatur nesciunt autem quaerat quidem repellendus nobis! Nobis maiores rerum tempora illo. Deserunt quaerat quo veniam corrupti sit?
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem commodi minus nihil quidem hic eius quas quasi expedita. Exercitationem magnam deserunt vel quisquam nulla soluta possimus neque eos voluptas architecto.
                            </div>
                            <div class="col s12 m4">
                                <img src="images/fond.jpg" class="responsive-img" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4">
                                <img src="images/fond.jpg" class="responsive-img" alt="">
                            </div>
                            <div class="col s12 m8" style='font-weight:200px;line-height:3;text-align:justify'>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio inventore quis nihil voluptates ex amet recusandae, deleniti sint, accusamus beatae possimus officia voluptatum quaerat dolorem facilis excepturi dolore nemo quibusdam!
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis pariatur impedit adipisci ad delectus dolore nulla doloribus soluta quam provident, asperiores placeat ratione quas eos nihil esse possimus atque sunt.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis animi nisi hic pariatur nesciunt autem quaerat quidem repellendus nobis! Nobis maiores rerum tempora illo. Deserunt quaerat quo veniam corrupti sit?
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem commodi minus nihil quidem hic eius quas quasi expedita. Exercitationem magnam deserunt vel quisquam nulla soluta possimus neque eos voluptas architecto.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b3746485a865abe"></script> 
        <?php include('pages/footer.php'); ?>
    </body>
</html>