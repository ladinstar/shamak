<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from hencework.com/theme/philbert/full-width-light/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 14:19:54 GMT -->
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Philbert I Fast build Admin dashboard for any platform</title>
	<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">

</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-1-active pimary-color-green">

      <!-- Top Menu Items -->
  		  <?php include("_menu.html") ?>
  		<!-- /Top Menu Items -->

      <!-- Left Sidebar Menu -->
  		  <?php include("_sidebar.html") ?>
      <!-- /Left Sidebar Menu -->


		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">

				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Actors</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="index.html">Dashboard</a></li>
						<li><a href="#"><span>table</span></a></li>
						<li class="active"><span>basic table</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">List of actors</h6>
								</div>

                <div class="pull-right">
                  <a href="acteur_add.php">
                    <button class="btn btn-success btn-anim">
                      <i class="fa fa-plus"></i>
                      <span class="btn-text">Add actors</span>
                    </button>
                  </a>
								</div>

							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<p class="text-muted">This is a list of actors in your database</p>
									<div class="table-wrap mt-40">
										<div class="table-responsive">
											<table class="table table-bordered table-hover mb-0">
												<thead>
												  <tr>
													<th>#</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Action</th>
												  </tr>
												</thead>
												<tbody>

                          <tr>
  													<td>1</td>
  													<td>Jens</td>
  													<td>Brincker</td>
  													<td class="text-nowrap">
                              <a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit">
                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                              </a>
                              <a href="#" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash text-danger"></i>
                              </a>
                            </td>
												  </tr>
                          <tr>
  													<td>1</td>
  													<td>Jens</td>
  													<td>Brincker</td>
  													<td class="text-nowrap">
                              <a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit">
                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                              </a>
                              <a href="#" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash text-danger"></i>
                              </a>
                            </td>
												  </tr>
                          <tr>
  													<td>1</td>
  													<td>Jens</td>
  													<td>Brincker</td>
  													<td class="text-nowrap">
                              <a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit">
                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                              </a>
                              <a href="#" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash text-danger"></i>
                              </a>
                            </td>
												  </tr>
                          <tr>
  													<td>1</td>
  													<td>Jens</td>
  													<td>Brincker</td>
  													<td class="text-nowrap">
                              <a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit">
                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                              </a>
                              <a href="#" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash text-danger"></i>
                              </a>
                            </td>
												  </tr>
                          <tr>
  													<td>1</td>
  													<td>Jens</td>
  													<td>Brincker</td>
  													<td class="text-nowrap">
                              <a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit">
                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                              </a>
                              <a href="#" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash text-danger"></i>
                              </a>
                            </td>
												  </tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>


			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>2017 &copy; Philbert. Pampered by Hencework</p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->

		</div>
		<!-- /Main Content -->

    </div>
    <!-- /#wrapper -->

	<!-- JavaScript -->

    <!-- jQuery -->
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Piety JavaScript -->
	<script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
	<script src="dist/js/peity-data.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>

	<!-- Owl JavaScript -->
	<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

	<!-- Switchery JavaScript -->
	<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>

	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>

</body>


<!-- Mirrored from hencework.com/theme/philbert/full-width-light/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 14:19:54 GMT -->
</html>
